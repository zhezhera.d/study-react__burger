import React, {Component} from 'react';
import PropTypes from 'prop-types';
import clases from './BurgerIngridient.css';

class BurgerIngridient extends Component {
  render() {
    let ingridient = null;
    switch (this.props.type) {
      case('bread-bottom'):
        ingridient = <div className={clases.BreadBottom}></div>;
        break;
      case('bread-top'):
        ingridient = (
          <div className={clases.BreadTop}>
            <div className={clases.Seeds1}></div>
            <div className={clases.Seeds2}></div>
          </div>
        );
        break;
      case('salad'):
        ingridient = <div className={clases.Salad}></div>;
        break;
      case('meat'):
        ingridient = <div className={clases.Meat}></div>;
        break;
      case('cheese'):
        ingridient = <div className={clases.Cheese}></div>;
        break;
      case('bacon'):
        ingridient = <div className={clases.Bacon}></div>;
        break;
      default:
        ingridient = null;
    }
    return ingridient;
  }
}
BurgerIngridient.propTypes={
  type: PropTypes.string.isRequired
};

export default BurgerIngridient;